<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth')->middleware('open_api_throttle:50,1')->uses('Api\V1\Auth@login');

Route::get('/job')->middleware('authenticated_api_throttle:100,1')->uses('Api\V1\Job@listJob');
Route::post('/job')->middleware('authenticated_api_throttle:100,1')->uses('Api\V1\Job@createJob');
Route::put('/job/{idJob}')->middleware('authenticated_api_throttle:100,1')->uses('Api\V1\Job@updateJob');
Route::get('/applicant')->middleware('authenticated_api_throttle:100,1')->uses('Api\V1\Applicant@listApplicant');
Route::get('/applicant/{idApplicant}')->middleware('authenticated_api_throttle:100,1')->uses('Api\V1\Applicant@getApplicant');
Route::post('/applicant')->middleware('authenticated_api_throttle:100,1')->uses('Api\V1\Applicant@createApplicant');
Route::put('/applicant/{idApplicant}')->middleware('authenticated_api_throttle:100,1')->uses('Api\V1\Applicant@updateApplicant');
Route::delete('/applicant/{idApplicant}')->middleware('authenticated_api_throttle:100,1')->uses('Api\V1\Applicant@deleteApplicant');
