<?php

namespace App\Http\Requests;

use App\Http\Controllers\Api\V1\Exception\ValidationException;
use App\Http\Services\AuthService;
use Illuminate\Http\Request;
use Validator;

class PostAuthRequest extends BaseRequest
{
    private $authService;

    public function __construct(Request $request, AuthService $authService)
    {
        parent::__construct($request);
        $this->authService = $authService;
    }

    /**
     * If the request is not valid, throw an error
     */
    public function validateRequest()
    {
        $parameters = [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ];
        $validation = Validator::make($this->request->all(), $parameters);
        if ($validation->fails()) {
            $errStr = json_encode($validation->failed());
            throw new ValidationException($validation);
        }
    }

    /**
     * Process the request
     */
    public function elaborateRequest()
    {
        return $this->authService->login($this->request);
    }
}
