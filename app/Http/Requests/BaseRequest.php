<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;

abstract class BaseRequest
{

    /**
     *  The client request
     */
    protected $request;

    /**
     * Initialize the base request by storing a copy of the client request in order to
     * process it later on
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    abstract public function validateRequest();

    abstract public function elaborateRequest();

}
