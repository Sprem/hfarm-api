<?php

namespace App\Http\Requests;

use App\Http\Controllers\Api\V1\Exception\ValidationException;
use App\Http\Services\JobService;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;

class PostJobRequest extends BaseRequest
{
    private $jobService;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->jobService = new JobService();
    }

    /**
     * If the request is not valid, throw an error
     */
    public function validateRequest()
    {
        $parameters = [
            'title' => 'required|max:50',
            'category' => ['required', Rule::in(['internal','external'])],
            'client_name' => ['required', Rule::requiredIf($this->request["category"] == 'external')],
            'description' => 'string|min:3',
            'starting_date' => 'date',
            'closing_date' => 'date',
            'experience' => 'string|min:3|max:45',
        ];
        $validation = Validator::make($this->request->all(), $parameters);
        if ($validation->fails()) {
            $errStr = json_encode($validation->failed());
            throw new ValidationException($validation);
        }
    }

    /**
     * Process the request
     */
    public function elaborateRequest()
    {
        return $this->jobService->postJob($this->request);
    }
}
