<?php

namespace App\Http\Requests;

use App\Http\Controllers\Api\V1\Exception\ValidationException;
use App\Http\Services\ApplicantService;
use Illuminate\Http\Request;
use Validator;
use Log;

class PostApplicantRequest extends BaseRequest
{
    private $applicantService;

    public function __construct(Request $request, ApplicantService $applicantService)
    {
        parent::__construct($request);
        $this->applicantService = $applicantService;
    }

    /**
     * If the request is not valid, throw an error
     */
    public function validateRequest()
    {
        $parameters = [
            'first_name' => 'required|string|min:3',
            'last_name' => 'required|string|min:3',
            'dob' => 'nullable|date',
            'country' => 'nullable|min:3',
            'email' => 'required|email',
            'role' => 'required|string|min:3',
            'seniority' => 'required|string|min:3'
        ];
        $validation = Validator::make($this->request->all(), $parameters);
        if ($validation->fails()) {
            $errStr = json_encode($validation->failed());
            throw new ValidationException($validation);
        }
    }

    /**
     * Process the request
     */
    public function elaborateRequest()
    {
        return $this->applicantService->createApplicant($this->request);
    }
}
