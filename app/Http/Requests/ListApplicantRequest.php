<?php

namespace App\Http\Requests;

use App\Http\Controllers\Api\V1\Exception\ValidationException;
use App\Http\Services\ApplicantService;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Log;

class ListApplicantRequest extends BaseRequest
{
    private $applicantService;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->applicantService = new ApplicantService();
    }

    /**
     * If the request is not valid, throw an error
     */
    public function validateRequest()
    {
        //Do nothing
    }

    /**
     * Process the request
     */
    public function elaborateRequest()
    {
        return $this->applicantService->listApplicant($this->request);
    }
}
