<?php

namespace App\Http\Requests;

use App\Http\Controllers\Api\V1\Exception\ValidationException;
use App\Http\Services\JobService;
use Illuminate\Http\Request;
use Validator;

class ListJobRequest extends BaseRequest
{
    private $jobService;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->jobService = new JobService();
    }

    /**
     * If the request is not valid, throw an error
     */
    public function validateRequest()
    {
        //No validation needed -> the api request has been already authenticated.
        //Return all the job for the given company
    }

    /**
     * Process the request
     */
    public function elaborateRequest()
    {
        return $this->jobService->listJob($this->request);
    }
}
