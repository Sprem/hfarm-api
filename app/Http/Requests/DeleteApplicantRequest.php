<?php

namespace App\Http\Requests;

use App\Http\Controllers\Api\V1\Exception\ValidationException;
use App\Http\Services\ApplicantService;
use Illuminate\Http\Request;
use Validator;

class DeleteApplicantRequest extends BaseRequest
{
    private $applicantService;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->applicantService = new ApplicantService();
    }

    /**
     * If the request is not valid, throw an error
     */
    public function validateRequest()
    {
        $parameters = [
            'id_applicant' => 'required|string|size:10'
        ];
        $validation = Validator::make($this->request->all(), $parameters);
        if ($validation->fails()) {
            $errStr = json_encode($validation->failed());
            throw new ValidationException($validation);
        }
    }

    /**
     * Process the request
     */
    public function elaborateRequest()
    {
        return $this->applicantService->deleteApplicant($this->request);
    }
}
