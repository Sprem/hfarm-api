<?php

namespace App\Http\Models\Dto;

use App\Http\Models\Dto\Contract\DtoMapperFromObj;

class ApplicantDTO implements DtoMapperFromObj
{

    public $firstName;
    public $lastName;
    public $createdAt;
    public $dob;
    public $cvPath;
    public $keywords;
    public $country;
    public $email;
    public $role;
    public $seniority;

    function formatResourceFromObj  ($applicantObject){

        $resource = [
            "id_applicant" => (string) $applicantObject->id_applicant,
            "created_at" => (string) $applicantObject->created_at,
            "first_name" => (string) $applicantObject->first_name,
            "last_name" => (string) $applicantObject->last_name,
            "email" => (string) $applicantObject->email,
            "dob" => isset($applicantObject->dob) ? (string) $applicantObject->dob : null,
            "cv_path" => isset($applicantObject->cv_path) ? (string) $applicantObject->cv_path : null,
            "keywords" => isset($applicantObject->keywords) ? (string) $applicantObject->keywords : null,
            "seniority" => isset($applicantObject->seniority) ? (string) $applicantObject->seniority : null,
            "role" => isset($applicantObject->role) ? (string) $applicantObject->role : null,
            "country" => isset($applicantObject->country) ? (string) $applicantObject->country : null,
            "client_name" => isset($applicantObject->client_name) ? (string) $applicantObject->client_name : null,
            "job_call_title" => isset($applicantObject->title) ? (string) $applicantObject->title : null,
            "id_application" => isset($applicantObject->id_application) ? (string) $applicantObject->id_application : null,
            "is_internal" => isset($applicantObject->is_internal) ? (boolean) $applicantObject->is_internal : null,
            "is_busy" => isset($applicantObject->title) ? true : false
        ];

        return $resource;

    }

}
