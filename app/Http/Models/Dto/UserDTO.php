<?php

namespace App\Http\Models\Dto;

class UserDTO
{

    public $idUser;
    public $name;
    public $email;
    public $password;
    public $idCompany;
    public $role;
    public $createdAt;

    function formatResourceFromObject($userObject){

        $resource = [
            "id_user" => (string) $applicantObject->id_user,
            "name" => (string) $applicantObject->name,
            "email" => (string) $applicantObject->email,
            "id_company" => (string) $applicantObject->id_company,
            "role" => (string) $applicantObject->role,
            "createdAt" => (string) $applicantObject->createdAt
        ];

        return $resource;

    }

}
