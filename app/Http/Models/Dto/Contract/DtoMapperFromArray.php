<?php

namespace App\Http\Models\Dto\Contract;

interface DtoMapperFromArray
{

    /**
     * @param $array is the array the must be formatted
     */
    public function formatResourceFromArray($array);
}
