<?php

namespace App\Http\Models\Dto\Contract;

interface DtoMapperFromObj
{

    /**
     * @param $array is the array the must be formatted
     */
    public function formatResourceFromObj($obj);
}
