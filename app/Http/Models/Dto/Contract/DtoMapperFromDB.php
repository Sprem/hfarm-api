<?php

namespace App\Http\Models\Dto\Contract;

interface DtoMapperFromDB
{

    /**
     * @param $array is the array the must be formatted
     */
    public function formatResourceFromDB($identifier = null);
}
