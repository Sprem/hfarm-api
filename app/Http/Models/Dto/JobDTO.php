<?php

namespace App\Http\Models\Dto;

use App\Http\Models\Dto\Contract\DtoMapperFromObj;

class JobDTO implements DtoMapperFromObj
{

    public $title;
    public $category;
    public $clientName;
    public $description;
    public $startingDate;
    public $closingDate;
    public $experience;

    function formatResourceFromObj($jobObject){

          $resource = [
            "id_job_call" => (string) $jobObject->id_job_call,
            "created_at" => (string) $jobObject->created_at,
            "title" => (string) $jobObject->title,
            "id_user" => (string) $jobObject->id_user,
            "client_name" => isset($jobObject->client) ? (string) $jobObject->client : null,
            "is_internal" => isset($jobObject->is_internal) ? (bool) $jobObject->is_internal : false,
            "description" => isset($jobObject->job_description) ? (string) $jobObject->job_description : null,
            "starting_date" => isset($jobObject->starting_date) ? (string) $jobObject->starting_date : null,
            "closing_date" => isset($jobObject->closing_date) ? (string) $jobObject->closing_date : null,
            "division" => isset($jobObject->division) ? (string) $jobObject->division : null,
            "experience_required" => isset($jobObject->experience_required) ? (string) $jobObject->experience_required : null
        ];

        return $resource;

    }

}
