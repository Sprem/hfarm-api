<?php

namespace App\Http\Models\Dao;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Models\Dto\UserDTO;
use App\Http\Controllers\Api\V1\Exception\ModelException;

class UserDAO extends Model
{

    /**
     * Simple DAO layer as access point to retrieve
     * data from the DB. Eloquent could be use.
     */
    public function getUserByEmail($email)
    {

        try{

            $users = DB::select('select * from User where email = ?', [$email]);

            if(null == $users) return;

            $user = $users[0];

            $userDTO = new UserDTO();

            $userDTO->idUser = $user->id_user;
            $userDTO->name = $user->name;
            $userDTO->email = $user->email;
            $userDTO->password = $user->password;
            $userDTO->idCompany = $user->id_company;
            $userDTO->role = $user->role;
            $userDTO->createdAt = $user->created_at;

            return $userDTO;

        }catch(\Exception $e){
        echo $e;
            throw (new ModelException(6000));
        }

    }


}
