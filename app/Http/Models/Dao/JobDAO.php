<?php

namespace App\Http\Models\Dao;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Models\Dto\JobDTO;
use App\Http\Controllers\Api\V1\Exception\ModelException;
use Log;

class JobDAO extends Model
{

    /**
     * Simple DAO layer as access point to retrieve
     * data from the DB. Eloquent could be use.
     */
    public function createJob($jobObject)
    {

        try{

            DB::table('JobCall')->insert($jobObject);

        }catch(\Exception $e){
            Log::error($e);
            throw (new ModelException(6000));
        }

    }

    public function listJobByCompany($idCompany)
    {

        try{

            $jobs = DB::select('select j.id_job_call, j.created_at, j.title, j.id_user, j.client, j.is_internal,
                                    j.job_description, j.starting_date, j.closing_date
                                    from JobCall j join User u on j.id_user = u.id_user
                                    where u.id_company = ? order by j.created_at desc', [$idCompany]);

            return $jobs;

        }catch(\Exception $e){
            throw (new ModelException(6000));
        }

    }

    public function getJobById($idJob)
    {

        try{

            $jobs = DB::select('select id_job_call, created_at, title, id_user, client,
                                    is_internal, job_description, starting_date, closing_date
                                    from JobCall where id_job_call = ?', [$idJob]);

            return $jobs[0];

        }catch(\Exception $e){
            throw (new ModelException(6000));
        }

    }

    public function updateJob($jobData,$idJob)
    {

        try{

            if (null != $jobData) {
                DB::table('JobCall')
                    ->where('id_job_call', $idJob)
                    ->update($jobData);
            }

        }catch(\Exception $e){
            throw (new ModelException(6000));
        }

    }

}
