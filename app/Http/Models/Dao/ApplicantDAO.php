<?php

namespace App\Http\Models\Dao;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Models\Dto\ApplicantDTO;
use App\Http\Controllers\Api\V1\Exception\ModelException;
use Log;

class ApplicantDAO extends Model
{

    /**
     * Simple DAO layer as access point to retrieve
     * data from the DB. Eloquent could be use.
     */
    public function createApplicant($applicantObject)
    {

        try{

            DB::table('Applicant')->insert($applicantObject);

        }catch(\Exception $e){
            throw (new ModelException(6000));
        }

    }

    public function getApplicantById($idApplicant)
    {

        try{

            $applicants = DB::select('select id_applicant, created_at, first_name, last_name,
                                        dob, cv_path, keywords, seniority, role, country, email, status
                                        from Applicant where id_applicant = ?', [$idApplicant]);

            return $applicants[0];

        }catch(\Exception $e){
            throw (new ModelException(6000));
        }

    }

    public function deleteApplicantById($idApplicant)
    {

        try{

            DB::delete('delete from Applicant where id_applicant = ?', [$idApplicant]);

        }catch(\Exception $e){
            throw (new ModelException(6000));
        }

    }

    public function listApplicantByCompany($idCompany)
    {

        try{

            $query = 'select j.client as client_name, j.title, j.is_internal, a.id_applicant, a.created_at, a.first_name,
                        a.last_name, a.dob, a.cv_path, a.keywords, a.seniority, a.role, a.country,  a.email, app.id_application
                        from Applicant a LEFT JOIN Application app on a.id_applicant = app.id_applicant
                        LEFT JOIN JobCall j on j.id_job_call = app.id_job_call where a.id_company = ?';

            $binding = [$idCompany];

            $applicants = DB::select($query,$binding);

            return $applicants;

        }catch(\Exception $e){
            throw (new ModelException(6000));
        }

    }

    public function updateApplicant($applicantData,$idApplicant)
    {

        try{

            if (null != $applicantData) {
                DB::table('Applicant')
                    ->where('id_applicant', $idApplicant)
                    ->update($applicantData);
            }

        }catch(\Exception $e){
            throw (new ModelException(6000));
        }

    }

}
