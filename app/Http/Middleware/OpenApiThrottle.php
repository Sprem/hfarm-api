<?php

namespace App\Http\Middleware;
use App\Http\Controllers\Api\V1\Exception\ExceptionFormatter;

use Closure;

class OpenApiThrottle extends \Illuminate\Routing\Middleware\ThrottleRequests
{

    private $exceptionFormatter;

    /**
     * Create a 'too many attempts' response.
     *
     * @param  string $key
     * @param  int $maxAttempts
     * @return \Illuminate\Http\Response
     */
    protected function buildResponse($key, $maxAttempts)
    {

        $message = [
            "code" => 5700,
            "message" => "Too many attempts, please slow down the request",
        ];

        $retryAfter = $this->limiter->availableIn($key);

        $headers = $this->addCustomHeaders(
            $maxAttempts,
            $this->calculateRemainingAttempts($key, $maxAttempts, $retryAfter),
            $retryAfter
        );

        return response()->json($message, 429)->withHeaders($headers);

    }

    /**
     * Add the limit header information to the given response.
     *
     * @param  int $maxAttempts
     * @param  int $remainingAttempts
     * @param  int|null $retryAfter
     * @return \Illuminate\Http\Response
     */
    protected function addCustomHeaders($maxAttempts, $remainingAttempts, $retryAfter = null)
    {
        $headers = [
            'X-RateLimit-Limit' => $maxAttempts,
            'X-RateLimit-Remaining' => $remainingAttempts,
        ];

        if (!is_null($retryAfter)) {
            $headers['Retry-After'] = $retryAfter;
            $headers['Content-Type'] = 'application/json';
        }

        return $headers;

    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $maxAttempts = 60, $decayMinutes = 1, $prefix = "")
    {

        $key = $this->resolveRequestSignature($request);

        //Check the deafult throttle limit
        if ($this->limiter->tooManyAttempts($key, $maxAttempts, $decayMinutes)) {
            return $this->buildResponse($key, $maxAttempts);
        }

        $response = $next($request);
        return $this->addHeaders(
            $response, $maxAttempts,
            $this->calculateRemainingAttempts($key, $maxAttempts)
        );

    }

    private function sendErrorMessage($code)
    {
        if(null == $this->exceptionFormatter)
            $this->exceptionFormatter = new ExceptionFormatter();
        return $this->exceptionFormatter->sendErrorResponse($code);
    }

}
