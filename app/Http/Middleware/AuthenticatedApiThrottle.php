<?php

namespace App\Http\Middleware;
use App\Http\Controllers\Api\V1\Exception\ExceptionFormatter;
use App\Http\Controllers\Api\V1\Network\AuthClient\DefaultAuthConnector;
use Illuminate\Support\Facades\Cookie;

use Closure;

class AuthenticatedApiThrottle extends \Illuminate\Routing\Middleware\ThrottleRequests
{

    private $exceptionFormatter;

    /**
     * Create a 'too many attempts' response.
     *
     * @param  string $key
     * @param  int $maxAttempts
     * @return \Illuminate\Http\Response
     */
    protected function buildResponse($key, $maxAttempts)
    {

        $message = [
            "code" => 5700,
            "message" => "Too many attempts, please slow down the request",
        ];

        $retryAfter = $this->limiter->availableIn($key);

        $headers = $this->addCustomHeaders(
            $maxAttempts,
            $this->calculateRemainingAttempts($key, $maxAttempts, $retryAfter),
            $retryAfter
        );

        return response()->json($message, 429)->withHeaders($headers);

    }

    /**
     * Add the limit header information to the given response.
     *
     * @param  int $maxAttempts
     * @param  int $remainingAttempts
     * @param  int|null $retryAfter
     * @return \Illuminate\Http\Response
     */
    protected function addCustomHeaders($maxAttempts, $remainingAttempts, $retryAfter = null)
    {
        $headers = [
            'X-RateLimit-Limit' => $maxAttempts,
            'X-RateLimit-Remaining' => $remainingAttempts,
        ];

        if (!is_null($retryAfter)) {
            $headers['Retry-After'] = $retryAfter;
            $headers['Content-Type'] = 'application/json';
        }

        return $headers;

    }

    private function checkJwt($jwt){

        $authorizationServerConnector = new DefaultAuthConnector();
        $payload = $authorizationServerConnector->checkToken($jwt);
        return $payload;

    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $maxAttempts = 60, $decayMinutes = 1, $prefix = "")
    {

        /**
         * Retrieve JWT and use the auth server to validate the request
         */
        $cookie = Cookie::get('jwt');
        $payload = $this->checkJwt($cookie);
        if(null == $cookie){
            return $this->sendErrorMessage(5104);
        }else if(!((bool)$payload["is_jwt_valid"])){
            return $this->sendErrorMessage(5102);
        }else if(!((bool)$payload["is_subscription_valid"])){
            return $this->sendErrorMessage(5105);
        }

        //Add user identity to the request object
        $request["id_user"] = $payload["id_user"];
        $request["id_company"] = $payload["id_company"];

        $key = $this->resolveRequestSignature($request);

        //Check the deafult throttle limit
        if ($this->limiter->tooManyAttempts($key, $maxAttempts, $decayMinutes)) {
            return $this->buildResponse($key, $maxAttempts);
        }

        $response = $next($request);
        return $this->addHeaders(
            $response, $maxAttempts,
            $this->calculateRemainingAttempts($key, $maxAttempts)
        );

    }

    private function sendErrorMessage($code)
    {
        if(null == $this->exceptionFormatter)
            $this->exceptionFormatter = new ExceptionFormatter();
        return $this->exceptionFormatter->sendErrorResponse($code);
    }

}
