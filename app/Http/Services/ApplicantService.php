<?php

namespace App\Http\Services;

use App\Http\Controllers\Api\V1\Exception\BusinessLogicException;
use App\Http\Controllers\Api\V1\Exception\ModelException;
use App\Http\Models\Dao\ApplicantDAO;
use App\Http\Models\Dto\ApplicantDTO;
use Carbon\Carbon;
use Log;

class ApplicantService extends BaseService
{

    private $applicantDAO;
    private $applicantDTO;

    public function __construct() {
        $this->applicantDAO = new ApplicantDAO();
        $this->applicantDTO = new ApplicantDTO();
    }

    public function listApplicant($request){

        try {

            //Retrieve data from DB
            $applicants = $this->applicantDAO->listApplicantByCompany($request["id_company"]);
            //Format resource and return it
            $result = [];

            foreach($applicants as $applicant){
               $result[] = $this->applicantDTO->formatResourceFromObj($applicant);
            }

            return response()->json($result, 200);

        } catch (ModelException $mException) {
            throw $mException;
        } catch (BusinessLogicException $bException) {
            throw $bException;
        } catch (\Exception $e) {
            throw new BusinessLogicException(5300);
        }

    }

    public function createApplicant($request)
    {
        try {

            $idApplicant =  $this->generateRandomString();
            //format date
            $this->formatDob($request);
            //Create obj to be stored
            $applicant = [
                "id_applicant" => $idApplicant,
                "id_company" => $request["id_company"],
                "email" => $request["email"],
                "first_name" => $request["first_name"],
                "last_name" => $request["last_name"],
                "role" => $request["role"],
                "seniority" => $request["seniority"],
                "country" => isset($request["country"]) ? $request["country"] : null,
                "dob" => isset($request["dob"]) ? $request["dob"] : null
            ];

            $jobs = $this->applicantDAO->createApplicant($applicant);
            $response = $this->applicantDTO->formatResourceFromObj($this->applicantDAO->getApplicantById($idApplicant));

            return response()->json($response, 201);

        } catch (ModelException $mException) {
            throw $mException;
        } catch (BusinessLogicException $bException) {
            throw $bException;
        } catch (\Exception $e) {
            throw new BusinessLogicException(5300);
        }
    }

    public function updateApplicant($request)
    {
        try {

            //Create obj to be updates
            $applicant = array();
            //format date
            $this->formatDob($request);
            if(isset($request["email"])) $applicant["email"] = $request["email"];
            if(isset($request["first_name"])) $applicant["first_name"] = $request["first_name"];
            if(isset($request["last_name"])) $applicant["last_name"] = $request["last_name"];
            if(isset($request["role"])) $applicant["role"] = $request["role"];
            if(isset($request["seniority"])) $applicant["seniority"] = $request["seniority"];
            if(isset($request["country"])) $applicant["country"] = $request["country"];
            if(isset($request["dob"])) $applicant["dob"] = $request["dob"];

            $this->applicantDAO->updateApplicant($applicant,$request["id_applicant"]);

            $response = $this->applicantDTO->formatResourceFromObj($this->applicantDAO->getApplicantById($request["id_applicant"]));

            return response()->json($response, 200);

        } catch (ModelException $mException) {
            throw $mException;
        } catch (BusinessLogicException $bException) {
            throw $bException;
        } catch (\Exception $e) {
            throw new BusinessLogicException(5300);
        }
    }

    public function getApplicant($request)
    {

        //Retrieve data from DB
        $applicant = $this->applicantDAO->getApplicantById($request["id_applicant"]);
        //Format resource and return it
        $response = $this->applicantDTO->formatResourceFromObj($applicant);

        return response()->json($response, 200);

    }

    public function deleteApplicant($request)
    {
        //Delete applicant from the db
        $applicant = $this->applicantDAO->deleteApplicantById($request["id_applicant"]);
        return response()->json(null, 200);
    }

    private function formatDob($request){
        if(isset($request["dob"])){
            $dob = Carbon::parse($request["dob"]);
            $request["dob"] = $dob->format('Y-m-d');
        }
    }

}
