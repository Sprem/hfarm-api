<?php

namespace App\Http\Services;

use App\Http\Controllers\Api\V1\Exception\BusinessLogicException;
use App\Http\Controllers\Api\V1\Exception\ExceptionFormatter;
use App\Http\Controllers\Api\V1\Exception\ModelException;
use App\Http\Controllers\Api\V1\Network\AuthClient\DefaultAuthConnector;
use App\Http\Models\Dao\UserDAO;

use Cookie;
use Hash;

class AuthService extends BaseService
{

    private $exceptionFormatter;
    private $authorizationServerConnector;
    private $userDAO;

    public function __construct(
        DefaultAuthConnector $authorizationServerConnector,
        UserDAO $userDAO,
        ExceptionFormatter $exceptionFormatter
    ) {
        $this->userDAO = $userDAO;
        $this->exceptionFormatter = $exceptionFormatter;
        $this->authorizationServerConnector = $authorizationServerConnector;
    }

    public function login($request)
    {
        try {

            $userDTO = $this->userDAO->getUserByEmail($request["email"]);
            if(null == $userDTO) throw new BusinessLogicException(5100);

            //User found: check his pwd
            if (Hash::check($request["password"], $userDTO->password)) {
                //Use the auth server connector to create a new JWT that will
                //be returned as cookie to the client
                $token = $this->authorizationServerConnector->createToken($userDTO->idCompany,$userDTO->idUser);
                //Set cookie and return
                return response()->json(null, 201)->cookie(
                                                        'jwt',
                                                        $token,
                                                        (60 * 24 * 365 * 10),
                                                        null,
                                                        null,
                                                        false,
                                                        true
                                                    );

            }else{
                throw new BusinessLogicException(5100); //Password is invalid
            }

        } catch (ModelException $mException) {
            throw $mException;
        } catch (BusinessLogicException $bException) {
            throw $bException;
        } catch (\Exception $e) {
            throw new BusinessLogicException(5300);
        }
    }

}
