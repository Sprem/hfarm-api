<?php

namespace App\Http\Services;

use App\Http\Controllers\Api\V1\Exception\BusinessLogicException;
use App\Http\Controllers\Api\V1\Exception\ModelException;
use App\Http\Models\Dao\JobDAO;
use App\Http\Models\Dto\JobDTO;
use Carbon\Carbon;
use Log;

class JobService extends BaseService
{

    private $jobDAO;
    private $jobDTO;

    public function __construct() {
        $this->jobDAO = new JobDAO();
        $this->jobDTO = new JobDTO();
    }

    public function listJob($request){

        try {

            //Retrieve data from DB
            $jobs = $this->jobDAO->listJobByCompany($request["id_company"]);
            //Format resource and return it
            $result = [];

            foreach($jobs as $job){
               $result[] = $this->jobDTO->formatResourceFromObj($job);
            }

            return response()->json($result, 200);

        } catch (ModelException $mException) {
            throw $mException;
        } catch (BusinessLogicException $bException) {
            throw $bException;
        } catch (\Exception $e) {
            throw new BusinessLogicException(5300);
        }

    }

    public function postJob($request)
    {
        try {

            $idJob =  $this->generateRandomString();
            //format date
            if(isset($request["starting_date"])){
                $startingDate = Carbon::parse($request["starting_date"]);
                $request["starting_date"] = $startingDate->format('Y-m-d');
            }
            if(isset($request["closing_date"])){
                $closingDate = Carbon::parse($request["closing_date"]);
                $request["closing_date"] = $closingDate->format('Y-m-d');
            }
            //Create obj to be stored
            $job = [
                "id_job_call" => $idJob,
                "title" => $request["title"],
                "id_user" => $request["id_user"],
                "client" => $request["client_name"],
                "is_internal" => ($request["category"] == "internal") ? 1 : 0,
                "job_description" => isset($request["description"]) ? $request["description"] : null,
                "starting_date" => isset($request["starting_date"]) ? $request["starting_date"] : null,
                "closing_date" => isset($request["closing_date"]) ? $request["closing_date"] : null,
                "experience_required" => isset($request["experience"]) ? $request["experience"] : null,
            ];

            $jobs = $this->jobDAO->createJob($job);
            $response = $this->jobDTO->formatResourceFromObj($this->jobDAO->getJobById($idJob));

            return response()->json($response, 201);

        } catch (ModelException $mException) {
            throw $mException;
        } catch (BusinessLogicException $bException) {
            throw $bException;
        } catch (\Exception $e) {
            Log::error($e);
            throw new BusinessLogicException(5300);
        }
    }

    public function updateJob($request)
    {
        try {

            //Create obj to be updates
            $job = array();
            if(isset($request["description"])) $job["job_description"] = $request["description"];
            if(isset($request["title"])) $job["title"] = $request["title"];
            if(isset($request["client_name"])) $job["client"] = $request["client_name"];
            if(isset($request["starting_date"])) $job["starting_date"] = $request["starting_date"];
            if(isset($request["closing_date"])) $job["closing_date"] = $request["closing_date"];
            if(isset($request["experience_required"])) $job["experience_required"] = $request["experience_required"];
            if(isset($request["category"])) $job["is_internal"] = ($request["category"] == "internal") ? 1 : 0;

            $this->jobDAO->updateJob($job,$request["id_job"]);

            $response = $this->jobDTO->formatResourceFromObj($this->jobDAO->getJobById($request["id_job"]));

            return response()->json($response, 200);

        } catch (ModelException $mException) {
            throw $mException;
        } catch (BusinessLogicException $bException) {
            throw $bException;
        } catch (\Exception $e) {
            throw new BusinessLogicException(5300);
        }
    }

}
