<?php

namespace App\Http\Core;

use App\Http\Controllers\Api\Th4Biz\UrlManager;
use App\Http\Controllers\Api\V1\Exception\BusinessLogicException;
use App\Http\Controllers\Api\V1\Exception\BusinessRulesException;
use App\Http\Controllers\Api\V1\Exception\ExceptionFormatter;
use App\Http\Controllers\Api\V1\Exception\ModelException;
use App\Http\Controllers\Api\V1\Exception\ValidationException;
use App\Http\Requests\BaseRequest;
use Log;

class DefaultCoreDispatcher implements CoreDispatcher
{

    private $exceptionFormatter;

    public function __construct(ExceptionFormatter $exceptionFormatter)
    {
        $this->exceptionFormatter = $exceptionFormatter;
    }

    /**
     * @Override base interface
     */
    public function processRequest(BaseRequest $request)
    {

        try {

            /*
             * At first we validate the request
             */
            $request->validateRequest();

            /*
             * Process the request.
             * The BaseRequest implementation is responsible to forward the request
             * to the right component.
             */
            return $request->elaborateRequest();

            /**
             * Below we handle all the different type of exception we may have from the
             * different layers of the application
             */
        } catch (ValidationException $validationException) {
            $this->handleException($validationException);
            return $this->exceptionFormatter->sendErrorResponse(5200, json_decode($validationException->getMessage()));
        } catch (BusinessRulesException $businessRulesException) {
            $this->handleException($businessRulesException);
            return $this->exceptionFormatter->sendErrorResponse($businessRulesException->getCode());
        } catch (BusinessLogicException $businessLogicException) {
            $this->handleException($businessLogicException);
            return $this->checkBusinessError($businessLogicException->getCode());
        } catch (ModelException $modelException) {
            $this->handleException($modelException);
            return $this->checkModelError($modelException->getCode());
        } catch (\Taskhunters\Exceptions\ModelException $modelException) {
            $this->handleException($modelException);
            return $this->checkModelError($modelException->getCode());
        } catch (\Exception $exception) {
            $this->handleException($exception);
            return $this->exceptionFormatter->sendErrorResponse(5300);
        }

    }

    private function checkModelError($errorCode)
    {

        if (null != $errorCode) {
            return $this->exceptionFormatter->sendErrorResponse($errorCode);
        } else {
            return $this->exceptionFormatter->sendErrorResponse(5300);
        }

    }

    private function checkBusinessError($errorCode)
    {

        if ($errorCode == 5500) {
            return redirect()->away(UrlManager::getConfirmStudentEmailFailureUrl());
        } else if (null != $errorCode) {
            return $this->exceptionFormatter->sendErrorResponse($errorCode);
        } else {
            return $this->exceptionFormatter->sendErrorResponse(5300);
        }

    }

    private function handleException($exception)
    {
        //Do something with the esception.
        //Either forward it to sentry, print it on syslog or whatever
        //is more convinient.
        Log::error($exception);
    }

}
