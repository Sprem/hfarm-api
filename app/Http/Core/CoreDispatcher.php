<?php

namespace App\Http\Core;

use App\Http\Requests\BaseRequest;

interface CoreDispatcher
{

    public function processRequest(BaseRequest $request);

}
