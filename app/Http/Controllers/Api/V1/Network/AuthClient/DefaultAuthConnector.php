<?php

namespace App\Http\Controllers\Api\V1\Network\AuthClient;

class DefaultAuthConnector
{

    public function checkToken($token){

        try {

            $client = new \GuzzleHttp\Client();
            $authorization = "Basic ".base64_encode(env('AUTH_CLIENT_ID').":".env('AUTH_SECRET_ID'));

            $response = $client->request('GET',
                                env('AUTH_CHECK_URL'), [
                                    'headers' => [
                                        'Authorization' => $authorization
                                    ],
                                    'query' => [
                                        'token' => $token
                                    ]
                                ]);

            $jsonBody = json_decode($response->getBody(), true);
            return $jsonBody;

        } catch (\Exception $e) {
            throw $e;
        }

    }

    public function createToken($idCompany,$idUser){

        try {

            $client = new \GuzzleHttp\Client();
            $authorization = "Basic ".base64_encode(env('AUTH_CLIENT_ID').":".env('AUTH_SECRET_ID'));

            $response = $client->request('POST',
                                env('AUTH_CREATE_URL'), [
                                    'headers' => [
                                        'Authorization' => $authorization
                                    ],
                                    'json' => [
                                        'id_company' => $idCompany,
                                        'id_user' => $idUser
                                    ]
                                ]);

            $jsonBody = json_decode($response->getBody(), true);

            return  $jsonBody["token"];

        } catch (\Exception $e) {
            throw $e;
        }

    }


}
