<?php

namespace App\Http\Controllers\Api\V1\Exception;

class ValidationException extends \Exception
{

    // Redefine the exception so message isn't optional
    public function __construct($err = null)
    {
        $desc = $err instanceof \Illuminate\Validation\Validator ? json_encode($err->errors()->getMessages()) : "";
        $code = is_int($err) ? ((int) $err) : 0;
        parent::__construct($desc, $code, null);
    }
}
