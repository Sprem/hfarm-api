<?php

namespace App\Http\Controllers\Api\V1\Exception;

class BusinessLogicException extends \Exception
{

    // Redefine the exception so message isn't optional
    public function __construct($code = 0)
    {
        parent::__construct("", $code, null);
    }

}
