<?php

namespace App\Http\Controllers\Api\V1\Exception;

/**
 * This trait provides helpufl methods for returning a formatted error message
 */
class ExceptionFormatter
{

    /**
     * @return a formatted error code with details about the exception
     */
    public function sendErrorResponse($customCode, $errors = [])
    {

        $json = [
            "code" => $customCode,
            "message" => self::getEnglishErrorMessage($customCode),
            "errors" => $errors,
        ];

        return response()->json(
            $json,
            $this->getHttpStatusCode($customCode)
        );
    }

    public function sendErrorModelResponse(ModelException $exception)
    {
        $json = [
            "code" => $exception->getCode(),
            "message" => self::getEnglishErrorMessage($exception->getCode()),
        ];

        return response()->json(
            $json,
            $this->getHttpStatusCode($exception->getCode())
        );
    }

    private function getHttpStatusCode($errorCode)
    {
        $codes = array(
            5100 => 401, //unhautorized
            5101 => 401, //unhautorized
            5102 => 401, //unhautorized
            5103 => 401, //unhautorized
            5200 => 400, //Bad request
            5300 => 500, //internal errors
            6000 => 500, //internal error
        );
        return (isset($codes[$errorCode])) ? $codes[$errorCode] : '500';
    }

    private function getEnglishErrorMessage($errorCode)
    {
        $codes = array(
            5100 => 'Invalid authentication credential, unauthorized',
            5101 => 'JWT is blacklisted',
            5102 => 'JWT not valid',
            5103 => 'JWT expired',
            5104 => 'JWT not found',
            5105 => 'Invalid subscription',
            5200 => 'Bad request',
            5300 => 'Internal server error',
            6000 => 'Internal server error: DAO exception',
        );
        return (isset($codes[$errorCode])) ? $codes[$errorCode] : '';
    }
}
