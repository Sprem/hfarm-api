<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

class Auth extends BaseController
{

    public function login(Request $request){

        try {

            $postAuthRequest = app()->make('\App\Http\Requests\PostAuthRequest', [$request]);
            return $this->defaultCoreDispatcher->processRequest($postAuthRequest);

        } catch (\Exception $e) {
            return $this->exceptionFormatter->sendErrorResponse(5300);
        }

    }

}
