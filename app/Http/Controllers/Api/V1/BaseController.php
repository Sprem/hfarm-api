<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\Exception\ExceptionFormatter;
use App\Http\Core\DefaultCoreDispatcher;
use Carbon\Carbon;
use Illuminate\Routing\Controller as BController;

class BaseController extends BController
{

    protected $defaultCoreDispatcher;
    protected $exceptionFormatter;

    public function __construct(DefaultCoreDispatcher $defaultCoreDispatcher, ExceptionFormatter $exceptionFormatter)
    {
        $this->defaultCoreDispatcher = $defaultCoreDispatcher;
        $this->exceptionFormatter = $exceptionFormatter;
    }

}
