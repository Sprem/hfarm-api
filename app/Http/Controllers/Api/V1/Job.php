<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Log;

class Job extends BaseController
{

    public function listJob(Request $request){

        try {

            $listJobRequest = app()->make('\App\Http\Requests\ListJobRequest', [$request]);
            return $this->defaultCoreDispatcher->processRequest($listJobRequest);

        } catch (\Exception $e) {
            return $this->exceptionFormatter->sendErrorResponse(5300);
        }

    }

    public function createJob(Request $request){

        try {

            $postJobRequest = app()->make('\App\Http\Requests\PostJobRequest', [$request]);
            return $this->defaultCoreDispatcher->processRequest($postJobRequest);

        } catch (\Exception $e) {
            return $this->exceptionFormatter->sendErrorResponse(5300);
        }

    }

    public function updateJob(Request $request,$idJob){

        try {

            $request["id_job"] = $idJob;
            $updateJobRequest = app()->make('\App\Http\Requests\UpdateJobRequest', [$request]);
            return $this->defaultCoreDispatcher->processRequest($updateJobRequest);

        } catch (\Exception $e) {
            return $this->exceptionFormatter->sendErrorResponse(5300);
        }

    }

}
