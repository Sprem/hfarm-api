<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

class Applicant extends BaseController
{

    public function listApplicant(Request $request){

        try {

            $listApplicantRequest = app()->make('\App\Http\Requests\ListApplicantRequest', [$request]);
            return $this->defaultCoreDispatcher->processRequest($listApplicantRequest);

        } catch (\Exception $e) {
            return $this->exceptionFormatter->sendErrorResponse(5300);
        }

    }

    public function getApplicant(Request $request, $idApplicant){

        try {

            $request["id_applicant"] = $idApplicant;
            $getApplicantRequest = app()->make('\App\Http\Requests\GetApplicantRequest', [$request]);
            return $this->defaultCoreDispatcher->processRequest($getApplicantRequest);

        } catch (\Exception $e) {
            return $this->exceptionFormatter->sendErrorResponse(5300);
        }

    }

    public function createApplicant(Request $request){

        try {

            $postApplicantRequest = app()->make('\App\Http\Requests\PostApplicantRequest', [$request]);
            return $this->defaultCoreDispatcher->processRequest($postApplicantRequest);

        } catch (\Exception $e) {
            return $this->exceptionFormatter->sendErrorResponse(5300);
        }

    }

    public function updateApplicant(Request $request, $idApplicant){

        try {

            $request["id_applicant"] = $idApplicant;
            $updateApplicantRequest = app()->make('\App\Http\Requests\UpdateApplicantRequest', [$request]);
            return $this->defaultCoreDispatcher->processRequest($updateApplicantRequest);

        } catch (\Exception $e) {
            return $this->exceptionFormatter->sendErrorResponse(5300);
        }

    }

    public function deleteApplicant(Request $request, $idApplicant){

        try {

            $request["id_applicant"] = $idApplicant;
            $deleteApplicantRequest = app()->make('\App\Http\Requests\DeleteApplicantRequest', [$request]);
            return $this->defaultCoreDispatcher->processRequest($deleteApplicantRequest);

        } catch (\Exception $e) {
            return $this->exceptionFormatter->sendErrorResponse(5300);
        }

    }

}
